import 'dart:io';

import 'character.dart';

class Player extends character {
  Player(super.name, super.maxHp, super.xp, super.hp);

  @override
  void attack() {
    print("$name attacked");
  }

  void skill() {
    print("$name used skill");
  }

  @override
  void defend() {
    print("$name defended");
  }

  @override
  void info() {
    print("name: $name maxHp: $maxHp hp: $hp  xp: $xp");
  }
}
