import 'dart:io';
import 'Player.dart';

class game {
  void gameStart() {
    Player player;
    String name;
    bool haveName = false;

    while (!haveName) {
      print("What's your name?");
      name = stdin.readLineSync()!;
      clearConsole();
      print("Your name is $name . Is that correct?");
      print("(1) Yes");
      print("(2) No,I changed my mind");
      int input = int.parse(stdin.readLineSync()!);
      clearConsole();
      if (input == 1) {
        haveName = true;
        player = Player(name, 100, 100, 0);
      }
      print(name);
    }
  }

  void clearConsole() {
    for (int i = 0; i < 100; i++) {
      print("");
    }
  }
}
