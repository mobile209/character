import 'dart:io';

import 'character.dart';

class Enemy extends character {
  Enemy(super.name, super.maxHp, super.hp, super.xp);

  @override
  void attack() {
    print("$name attacked");
  }

  @override
  void defend() {
    print("$name defended");
  }

  void info() {
    print("name: $name maxHp: $maxHp hp: $hp  xp: $xp");
  }
}
