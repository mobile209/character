import 'dart:io';

abstract class character {
  String name;
  int maxHp, hp, xp;

  character(this.name, this.maxHp, this.xp, this.hp);

  void attack() {
    print("$name attacked");
  }

  void defend() {
    print("$name attacked");
  }

  String get getName {
    return name;
  }

  set setName(String newName) {
    name = newName;
  }

  int get getMaxHp {
    return maxHp;
  }

  set setMaxHp(int newMaxHp) {
    maxHp = newMaxHp;
  }

  int get getHp {
    return hp;
  }

  set setHp(int newHp) {
    hp = newHp;
  }

  int get getXp {
    return xp;
  }

  set setXp(int newXp) {
    xp = newXp;
  }

  void info() {
    print("name: $name maxHp: $maxHp hp: $hp  xp: $xp");
  }
}
